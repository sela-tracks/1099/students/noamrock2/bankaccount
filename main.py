from BankAccount import BankAccount
import json

with open("Backup.json", "r") as f:
    bankAccountsDict = json.load(f)
    for bankAccountID in bankAccountsDict:
        BankAccount(bankAccountsDict[bankAccountID]['name'], int(bankAccountID), bankAccountsDict[bankAccountID]['balance'], bankAccountsDict[bankAccountID]['activities'])

choice = input("Welcome to TarBank!\nWhat would you like to do?\n\n1. Add new Account\n2. Choose account\n\n")
if choice == "1":
    name = input("Great, whats your name?\n")
    identity = int(input("Whats your identity?\n"))
    balance = int(input ("How much money do you want to deposit?\n"))

    BankAccount(name, identity, balance, [])
    with open("Backup.json", "r") as f:
        bankAccountsDict = json.load(f)

    bankAccountsDict[identity] = {"name": name, "balance": balance, "activities": []}
    with open("Backup.json", "w") as f:
        json.dump(bankAccountsDict, f)

elif choice == "2":
    Userid = int(input("what is your id?\n"))
    userBankAccount = BankAccount.bank_accounts[Userid]
    choice = input(f"Hello {userBankAccount.name}! What would you like to do?\n1. Deposit funds\n2. Withdraw funds\n3. Transfer funds\n4. My activities\n\n")
    if choice == "1":
        amount = int(input("How much do you want to deposit?\n"))
        userBankAccount.deposit(amount)

        with open("Backup.json", "r") as f:
            bankAccountsDict = json.load(f)
        bankAccountsDict[str(Userid)] = {"name": userBankAccount.name, "balance": userBankAccount.balance, "activities": userBankAccount.activities}
        with open("Backup.json", "w") as f:
            json.dump(bankAccountsDict, f)

        print(f"I have deposited {amount} to your account.\nYour new balance is {userBankAccount.balance}.\nHave a nice day")

    elif choice =="2":
        amount = int(input("How much do you want to withdraw?\n"))
        userBankAccount.withdraw(amount)

        with open("Backup.json", "r") as f:
            bankAccountsDict = json.load(f)
        bankAccountsDict[str(Userid)] = {"name": userBankAccount.name, "balance": userBankAccount.balance, "activities": userBankAccount.activities}
        with open("Backup.json", "w") as f:
            json.dump(bankAccountsDict, f)

        print(f"you withdrew {amount}\nYour new balance is {userBankAccount.balance}.\nHave a nice day")

    elif choice =="3":
        identity = int(input("Insert the identity of the account you want to transfer to\n"))
        amount = int(input("How much money do you want to transfer?\n"))
        userBankAccount.transfer_balance(identity, amount)

        with open("Backup.json", "r") as f:
            bankAccountsDict = json.load(f)
        bankAccountsDict[str(Userid)] = {"name": userBankAccount.name, "balance": userBankAccount.balance, "activities": userBankAccount.activities}
        toBankAcount = BankAccount.bank_accounts[identity]
        bankAccountsDict[str(identity)] = {"name": toBankAcount.name, "balance": toBankAcount.balance, "activities": toBankAcount.activities}
        with open("Backup.json", "w") as f:
            json.dump(bankAccountsDict, f)

        print(f"you transfered {amount} to {BankAccount.bank_accounts[identity].name}")

    elif choice =="4":
        userBankAccount.my_activities()


else:
    print("Wrong input")

