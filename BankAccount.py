class BankAccount:
    fee = 10
    bank_accounts = {}
    def __init__(self, name, identity, balance, activities):
        self.name = name
        self.identity = identity
        self.balance = balance
        self.activities = activities

        BankAccount.bank_accounts[identity] = self


    def __str__(self):
        return f"This is {self.name}'s bank account, he has ID {self.identity} and he currently has {self.balance} ILS in his account"

    def deposit(self, amount):
        self.balance += amount*(1-(BankAccount.fee/100))
        self.activities.append(f"{self.name} deposited {amount}")

    def withdraw(self, amount):
        self.balance -= amount*(1+(BankAccount.fee/100))
        self.activities.append(f"{self.name} withdrew {amount}")

    def transfer_balance(self, identity, amount):
        self.balance -= amount
        BankAccount.bank_accounts[identity].balance += amount
        self.activities.append(f"{self.name} transferred {amount} to {BankAccount.bank_accounts[identity].name}")


    def my_activities(self):
        print(self.activities)



